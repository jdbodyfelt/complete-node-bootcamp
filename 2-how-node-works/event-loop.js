const fs = require("fs");
const crypto = require("crypto");

const start = Date.now();
process.env.UV_THREADPOOL_SIZE = 4;

setTimeout(() => console.log("Timer 1 finished"), 0);
setImmediate(() => console.log("Immediate 1 finished"));

fs.readFile("test-file.txt", () => {
  console.log("I/O Finished");
  console.log("--------------");

  setTimeout(() => console.log("Timer 2 finished"), 0);
  setTimeout(() => console.log("Timer 3 finished"), 3000);
  setImmediate(() => console.log("Immediate 2 finished"));

  process.nextTick(() => console.log("Process the next tick"));

  crypto.pbkdf2Sync("password", "salty-bitch", 100000, 1024, "sha512");
  console.log((Date.now() - start) / 1000, "Password encrypted sync");

  crypto.pbkdf2Sync("password", "salty-bitch", 100000, 1024, "sha512");
  console.log((Date.now() - start) / 1000, "Password encrypted sync");

  crypto.pbkdf2("password", "salty-bitch", 100000, 1024, "sha512", () => {
    console.log((Date.now() - start) / 1000, "Password encrypted 3");
  });

  crypto.pbkdf2("password", "salty-bitch", 100000, 1024, "sha512", () => {
    console.log((Date.now() - start) / 1000, "Password encrypted 4");
  });

  crypto.pbkdf2("password", "salty-bitch", 100000, 1024, "sha512", () => {
    console.log((Date.now() - start) / 1000, "Password encrypted 5");
  });
});

console.log("Hello from the top-level code");
