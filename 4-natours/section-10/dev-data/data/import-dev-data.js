const fs = require('fs');
const mongoose = require('mongoose');
const Tour = require('./../../models/tourModel');
const dotenv = require('dotenv');

dotenv.config({
    path: './config.env'
});
console.log(process.env)

// Set up MongoDB Server

const DSN = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWD}@${process.env.DB_HOST}/${process.env.DB_NAME}`
console.log(DSN)
// Connect MongoDB
mongoose
    .connect(DSN, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    })
    .then(() => console.log('DB connection successful!'))
    .catch(error => console.log(error));

const tours = JSON.parse(fs.readFileSync(`${__dirname}/tours-simple.json`, 'utf-8'));

// Import data into database
const importData = async () => {
    try {
        await Tour.create(tours);
        console.log('Data successfully loaded!')
    } catch (err) {
        console.log(err);
    }
    process.exit();
}

// Delete all data from database
const deleteData = async () => {
    try {
        await Tour.deleteMany();
        console.log('Data successfully deleted!')
    } catch (err) {
        console.log(err);
    }
    process.exit();
}

if (process.argv[2] === '--import') {
    importData()
} else if (process.argv[2] === '--delete') {
    deleteData()

}