const express = require('express');
const router = express.Router();
const userCtl = require('./../controllers/userController');
const authCtl = require('./../controllers/authController');

router.post('/signup', authCtl.signup);
router.post('/login', authCtl.login);

router.post('/forgotPassword', authCtl.forgotPassword);
router.patch('/resetPassword/:token', authCtl.resetPassword);
router.patch('/updateMyPassword', authCtl.protect, authCtl.updatePassword);
router.patch('/updateMe', authCtl.protect, userCtl.updateMe);
router.delete('/deleteMe', authCtl.protect, userCtl.deleteMe);

router
  .route('/')
  .get(userCtl.getAllUsers)
  .post(userCtl.createUser);
router
  .route('/:id')
  .get(userCtl.getUser)
  .patch(userCtl.updateUser)
  .delete(userCtl.deleteUser);

module.exports = router;