const express = require('express');
const router = express.Router();
const userCtl = require('./../controllers/userController');
const authCtl = require('./../controllers/authController');

router.post('/signup', authCtl.signup);
router.post('/login', authCtl.login);
router.post('/forgotPassword', authCtl.forgotPassword);
router.patch('/resetPassword/:token', authCtl.resetPassword);

router.use(authCtl.protect); // Middleware protection for everythng after!
router.patch('/updateMyPassword', authCtl.updatePassword);
router.get('/me', userCtl.getMe, userCtl.getUser);
router.patch('/updateMe', userCtl.updateMe);
router.delete('/deleteMe', userCtl.deleteMe);

router.use(authCtl.restrictTo('admin'));
router
  .route('/')
  .get(userCtl.getAllUsers)
  .post(userCtl.createUser);

router
  .route('/:id')
  .get(userCtl.getUser)
  .patch(userCtl.updateUser)
  .delete(userCtl.deleteUser);

module.exports = router;
