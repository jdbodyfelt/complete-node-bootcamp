/* eslint-disable */
export const displayMap = locations => {
  mapboxgl.accessToken =
    'pk.eyJ1IjoiamRib2R5ZmVsdCIsImEiOiJjazU3a3RwMWgwNTA1M2Zwc2Z4b2xncnZyIn0.e4BHc2Ria135ZEDGb2L03w';

  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/jdbodyfelt/ck57l7wrb0jsu1cphmi178qo3',
    scrollZoom: false
    //   center: [-118, 34],
    //   zoom: 10,
    //   interactive: false
  });

  const bounds = new mapboxgl.LngLatBounds();
  locations.forEach(loc => {
    // Create marker
    const el = document.createElement('div');

    // Add info popup
    new mapboxgl.Popup({})
      .setLngLat(loc.coordinates)
      .setHTML(`<p>Day ${loc.day}: ${loc.description}</p>`)
      .addTo(map);
    el.className = 'marker';
    // Add marker
    new mapboxgl.Marker({
      element: el,
      anchor: 'bottom'
    })
      .setLngLat(loc.coordinates)
      .addTo(map);
    // Extend boundaries to include location
    bounds.extend(loc.coordinates);
  });

  map.fitBounds(bounds, {
    padding: {
      top: 200,
      bottom: 200,
      left: 100,
      right: 100
    }
  });
};
