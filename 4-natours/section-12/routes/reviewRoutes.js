const reviewCtl = require('./../controllers/reviewController');
const authCtl = require('./../controllers/authController');
const express = require('express');
const router = express.Router({ mergeParams: true });

router.use(authCtl.protect);
router
  .route('/')
  .get(reviewCtl.getAllReviews)
  .post(
    authCtl.restrictTo('user'),
    reviewCtl.setTourUserIds,
    reviewCtl.createReview
  );

router
  .route('/:id')
  .get(reviewCtl.getReview)
  .delete(authCtl.restrictTo('user', 'admin'), reviewCtl.deleteReview)
  .patch(authCtl.restrictTo('user', 'admin'), reviewCtl.updateReview);

module.exports = router;
