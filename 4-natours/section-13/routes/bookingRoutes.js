const bookCtl = require('./../controllers/bookingController');
const authCtl = require('./../controllers/authController');
const express = require('express');
const router = express.Router();

router.use(authCtl.protect);

router.get('/checkout-session/:tourId', bookCtl.getCheckoutSession);

router.use(authCtl.restrictTo('admin', 'lead-guide'));

router
  .route('/')
  .get(bookCtl.getAllBookings)
  .post(bookCtl.createBooking);

router
  .route('/:id')
  .get(bookCtl.getBooking)
  .patch(bookCtl.updateBooking)
  .delete(bookCtl.deleteBooking);

module.exports = router;
