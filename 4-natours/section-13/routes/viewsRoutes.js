const express = require('express');
const viewsCtl = require('../controllers/viewsController');
const authCtl = require('../controllers/authController');
const bookCtl = require('../controllers/bookingController');

const router = express.Router();

router.get(
  '/',
  bookCtl.createBookingCheckout,
  authCtl.isLoggedIn,
  viewsCtl.getOverview
);
router.get('/tour/:slug', authCtl.isLoggedIn, viewsCtl.getTour);
router.get('/login', authCtl.isLoggedIn, viewsCtl.getLoginForm);
router.get('/me', authCtl.protect, viewsCtl.getAccount);
router.get('/my-tours', authCtl.protect, viewsCtl.getMyTours);

router.post('/submit-user-data', authCtl.protect, viewsCtl.updateUserData);
module.exports = router;
