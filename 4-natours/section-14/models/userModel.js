const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

// Build the schema
const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please tell us your name!']
  },
  email: {
    type: String,
    required: [true, 'Please provide your email'],
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, 'Please provide a valid email']
  },
  photo: {
    type: String,
    default: 'default.jpg'
  },
  role: {
    type: String,
    enum: ['user', 'guide', 'lead-guide', 'admin'],
    default: 'user'
  },
  password: {
    type: String,
    required: [true, 'A user must have a password'],
    minlength: 8,
    select: false
  },
  passwordConfirm: {
    type: String,
    required: [true, 'Please confirm your password'],
    validate: {
      // This only works on DB's CREATE or SAVE!
      validator: function(el) {
        return el === this.password;
      },
      message: 'Passwords are NOT the same!'
    }
  },
  passwordChangedAt: Date,
  passwordResetToken: String,
  passwordResetExpiry: Date,
  active: {
    type: Boolean,
    default: true,
    select: false
  }
});

// Implement password encryption at middleware
userSchema.pre('save', async function(next) {
  // Only run on modifed password
  if (!this.isModified('password')) return next();
  // Hash the password with cost of 12 (Higher, more security but longer CPU)
  this.password = await bcrypt.hash(this.password, 12);
  // Delete passwordConfirm field
  // Note "required" is for input, NOT persistence!
  this.passwordConfirm = undefined; //
  next();
});

userSchema.pre('save', function(next) {
  if (!this.isModified('password') || this.isNew) return next();
  this.passwordChangedAt = Date.now() - 1000; // "One second in the past" hack
  next();
});

userSchema.pre(/^find/, function(next) {
  // this points to the current query!
  this.find({ active: { $ne: false } });
  next();
});

// Instance Methods
userSchema.methods.verifyPassword = async function(candidatePassword) {
  return await bcrypt.compare(candidatePassword, this.password);
};

userSchema.methods.changedPasswordAfter = function(timestampJWT) {
  if (this.passwordChangedAt) {
    const changedTimestamp = parseInt(
      this.passwordChangedAt.getTime() / 1000,
      10
    );
    return timestampJWT < changedTimestamp;
  }
  // False means NOT changed
  return false;
};

userSchema.methods.createPasswordResetToken = function(lifetime_mins) {
  // No need to get level of encryption as in password storage.
  const resetToken = crypto.randomBytes(32).toString('hex');
  this.passwordResetToken = crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');
  this.passwordResetExpiry = Date.now() + lifetime_mins * 60 * 1000;
  // console.log({ resetToken }, this.passwordResetToken);
  return resetToken;
};

// Link the schema to a class
const User = mongoose.model('User', userSchema);
module.exports = User;
