const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config({
  path: './config.env'
});

// Uncaught Exception Handler
process.on('uncaughtException', err => {
  console.log(err.name, err.message);
  console.log('UNCAUGHT EXCEPTION! Shutting down.');
  process.exit(1);
});

// Set up MongoDB Server
const DSN = 'mongodb+srv://USR:PWD@HOST/NAME?retryWrites=true&w=majority'
  .replace('USR', process.env.DB_USER)
  .replace('PWD', process.env.DB_PASSWD)
  .replace('HOST', process.env.DB_HOST)
  .replace('NAME', process.env.DB_NAME);

// Connect MongoDB
mongoose
  .connect(DSN, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log('DB connection successful!'));
// .catch(error => console.log(error));

const app = require('./app');
//console.log(process.env);

const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});

process.on('unhandledRejection', err => {
  console.log(err.name, err.message);
  console.log('UNHANDLED REJECTION! Shutting down.');
  server.close(() => {
    process.exit(1);
  });
});

process.on('SIGTERM', () => {
  console.log('☠️ SIGTERM RECEIVED. SHUTTING DOWN GRACEFULLY.');
  server.close(() => {
    console.log('⚰️ Process terminated!');
  });
});
