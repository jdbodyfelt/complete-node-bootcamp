const express = require('express');
const router = express.Router();
const {
  checkID,
  checkBody,
  getAllTours,
  createTour,
  getTour,
  updateTour,
  deleteTour
} = require('./../controllers/tourController');

router.param('id', checkID);

// Create a checkBody middleware
// Check if contains name and price property
// If not, send back 400 (bad request)
// Add to post handler stack

router
  .route('/')
  .get(getAllTours)
  .post(checkBody, createTour);
router
  .route('/:id')
  .get(getTour)
  .patch(updateTour)
  .delete(deleteTour);

module.exports = router;
