# complete-node-bootcamp

**PURPOSE:**
To upgrade my frontend/backend skillset from Qt+ to a more modern MERN stack.

**DESCRIPTION:**
This repository stores my progress of Jonas Schmedtmann's course _Node.js, Express, MongoDB, & More: The Complete Bootcamp 2020_.
More information and details are [available from the Udemy course site](https://www.udemy.com/course/nodejs-express-mongodb-bootcamp/).

**RESOURCES:**
The course assumes users have rudimentary knowledge of javascript. Some useful resources to get "up to speed"

- [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [The Modern JavaScript Tutorial](https://javascript.info)
- [Node Package Manager](https://www.npmjs.com)

Other than that, J. Schmedtmann does a super job giving us resources! The two I found myself going back to over and over were

- One of the first lecture slides that provided the course's [Github repo](https://github.com/jonasschmedtmann/complete-node-bootcamp),
- [Jona's own professional site](http://www.jonas.io) - there are some serious goodies there!

**MISC NOTES:**

- In Section 6, you'll need to have [Postman installed](http://www.getpostman.com) for API development.
- My build for Natour's Postman collection<br>
  [![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/2c74bd0382810cdc79ef)
- In Sec.2.68, run `npm i eslint prettier eslint-config-prettier eslint-plugin-prettier eslint-config-airbnb eslint-plugin-node eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react --save-dev`
- Sec. 11 was a bit rough. Lots of material, and felt like a "miscellaneous" lecture dump - ought be split into other sections. Jonas made a LOT of mistakes, and many were NOT teaching exemplars.
- Again, Sec. 11 issue: Enterprise requirements for Compass' "Schema" feature access is poor practice in teaching.
- While it's nice to have while developing, for the videos the vscode popover hides code sometimes...makes difficult w/ stopping video and looking back.
- Get rid of the chewing gum when you record lectures.
- Last half of section 12 was VERY rushed, just way TOO many mistakes being made that made it VERY hard to follow along with the lecture. As examples, the {} object problem of Sec.12.188, the
  `router.use(authController.isLoggedIn)` problem of Sect.12.189, the three tries to get parcel-bundler...
- "Uh, um, OK, but actually..." UGH. Avoid these words in lectures.
- "There's something wrong going on here..."
- "These types of things happen all the time." Only if you're a fuck-up.
- Needs a chapter on architecting the authorization pages. Started good with MVC, but then in Sec. 12 feels like he's just hacking code, especially with the err vs. error in production.
- You could trim the 6 hours of Section 12 down to about 4...
- Section 13: `natours.io` vs. `example.com`, `session` vs. `sessions`, bundler and stripe issue, stripe receipt email sends (section 14 ctl-C vs cmd-C, mistakes on Heroku)
- Way too many fuckups.

## My Challenge Solutions

Here, I discuss my personal solutions to J. Schmedtmann's issued challenges within the course.

1. **Slugify Product URLs**<br>
   For the first project _node-farm_, this was issued in Section 2.20. Initially, I thought of using a hashmap (_dictionary_ for my fellow Pythoneers), but
   realized I already have a similar object in existence, so just append a new field! The mission was accomplished with the following steps:

   A. Rather than use `dataObj`'s `query.id`, I create a new field immediately after our the dataload in `index.js`:

   ```javascript
   const dataObj = JSON.parse(data);
   dataObj.map(element => {
     element['slug'] = slugify(element.productName, { lower: true });
   });
   ```

   B. I then update `template-card.html`'s placeholder link:

   ```html
   <a class="card__link" href="/product?id={%ID%}"></a>
   ```

   becomes

   ```html
   <a class="card__link" href="/product/{%SLUG%}"></a>
   ```

   C. The placeholder's replacement needs updated in `modules/replaceTemplate.js`:

   ```javascript
   output = output.replace(/{%SLUG%}/g, product.slug);
   ```

   D. Finally, I note that in conditional's routing branch to a product page, I can make use of javascript's nice `.include()` function for substring checks, as well as using one of the Holy Trinity (that's `map, filter, reduce` for the unbelievers). My completed branch then looks like

   ```javascript
   } else if (pathname.includes('/product')) {
   res.writeHead(200, { 'Content-type': 'text/html' });
   const slug = pathname.replace('/product/', '');
   const product = dataObj.filter(element => {
   return element.slug === slug;
   })[0];
   const output = replaceTemplate(tempProduct, product);
   res.end(output);
   ```

   _Challenge Summary_: A pro to this approach is no need for key-mapping (use an existing object), nor need for multiconditionals (like some other posted solutions).
   A con is seen in the branch above, where I make use of a "dirty" array-to-scalar `product[0]`. This is acceptable for the assignment, since I know
   that the data is 100% unique, but in general that may not be the case. I'm sure there's a better way (there always is!), and I'd love to hear from more experienced MERN folk about this solution.

Following four challenges were issued at the end of Section 13:

2. **Restrict User Review to Only Booked Tours**

3. **Nested Booking Routes**

   - All bookings under one tour: `/tours/:id/bookings
   - All bookings under one user: `/users/:id/bookings`

4. **Improved Tour Dates**

   - Add `participants` and `soldOut` fields to each date. Date becomes like tour instance. Booking needs to select a date. New booking increments `participants` until booked out `participants === maxGroupSize`.

5. **Advanced Authentications**

   - Confirm user email.
   - Use refresh tokens to keep users logged in.
   - Two-factor authentication.

6. **Implement a Signup Form**

   - Very similar to login form.

7. **Add Review Directly to Website**

   - On tour detail page, if user taken tour, allow them to add review directly to website. Implement a form.

8. **Hide & Protect Booking**

   - On tour detail page, hide entire booking section if current user has already booked tour.
   - Prevent duplicates in model, like in reviews.

9. **Implement 'Like Tour'**

   - With "favorite tour" page

10. **Build "My Reviews" Page**

    - All reviews displayed, and user can edit them. Good for React practice!

11. **Build "Manage" Pages**

    - CRUD thru forms - for tours, reviews, users, & bookings for administrators
